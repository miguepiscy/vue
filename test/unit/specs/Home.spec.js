import Home from '@/views/Home';
import { shallowMount } from '@vue/test-utils';

describe('Home.vue', () => {
  let wrapper;
  beforeEach(() => {
    console.log('before');
    wrapper = shallowMount(Home);
  });
  it('should render correct contents', () => {
    console.log('it');
    expect(wrapper.find('.home').text())
      .toBe('Home');
  });
  // it('should render correct contents', () => {
  //   const Constructor = Vue.extend(Home);
  //   const vm = new Constructor().$mount();
  //   expect(vm.$el.querySelector('.home').textContent)
  //     .toEqual('Home');
  // });

  it('has correct default products', () => {
    console.log('it');
    expect(typeof Home.data).toBe('function');
    const defaultData = Home.data();
    expect(defaultData.products).toEqual([]);
  });

  it('check list', () => {
    wrapper.setData({
      products: [
        { id: 1, name: 'Iphone', price: 600, stock: 3 },
        { id: 2, name: 'Iphone', price: 600, stock: 3 },
      ],
    });
    expect(wrapper.findAll('li').length).toBe(2);
    console.log(wrapper.html());
  });

  it('check list', () => {
    //  .exists().tobe
    expect(wrapper.findAll('ul').length).toBe(0);
  });
});

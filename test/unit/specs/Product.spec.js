import Product from '@/components/Product';
import { shallowMount } from '@vue/test-utils';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const product = {
  id: 33,
  name: 'name',
  price: 100,
  stock: 1,
};
const actions = {
  addToCart: jest.fn(),
};
const store = new Vuex.Store({
  state: {},
  actions,
});

describe('Product.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(Product, {
      store,
      propsData: {
        product,
      },
    });
  });
  it('should render corrent', () => {
    expect(wrapper.find('.product__name').text())
      .toBe(product.name);
    expect(wrapper.find('.product__stock').text())
      .toBe(`${product.stock}`);
  });

  it('check price', () => {
    const priceEur = `${product.price} €`;
    expect(wrapper.find('.product__price').text())
      .toBe(priceEur);
    expect(wrapper.vm.price).toBe(priceEur);
  });

  it('addToCart', () => {
    const button = wrapper.find('button');
    button.trigger('click');
    const calls = actions.addToCart.mock.calls;
    expect(actions.addToCart).toBeCalled();
    expect(calls.length).toBe(1);
    console.log(calls[0]);
    expect(calls[0][1]).toBe(product.id);
  });
});

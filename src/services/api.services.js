import StockError from '@/errors/StockError';
import axios from 'axios';

const productos = [
  { id: 1, name: 'Iphone', price: 600, stock: 3 },
  { id: 2, name: 'Ipad', price: 300, stock: 2 },
  { id: 3, name: 'Mac', price: 1000, stock: 4 },
];

const cart = [];
// [ { itemId, units } ]
//  url/api/products

function getProducts() {
  return axios.get('http://api.myjson.com/bins/1401lb')
    .then(({ data }) => data);
  // [ 1, 2, 3 ]
}

// url/api/:id
function getProduct(id) {
  return new Promise((resolve) => {
    const product = productos.find(p => p.id === id);
    return resolve(product);
  });
}

function getCart() {
  return new Promise((resolve) => {
    resolve(cart);
  });
}

function checkout(cart) {
  return getProducts()
    .then(fillProductsIdWithCart) // eslint-disable-line no-use-before-define
    .then(throwErrorIfBadStock); // eslint-disable-line no-use-before-define
}

function throwErrorIfBadStock(products) {
  const findFail = products.find(({ product, cartUnit }) => product.stock < cartUnit);;
  if (findFail) {
    throw new StockError(findFail.id);
  }
  return 'ok';
}
function fillProductsIdWithCart(productsId) {
  const promises = productsId.map((id) => {
    const cartP = cart.find(element => element.itemId === id);
    return getProduct(id)
      .then((product) => {
        return {
          product,
          cartUnit: cartP.units,
        };
      });
  });
  return Promise.all(promises);
}

export default {
  getProducts,
  getProduct,
};

import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home';
import Checkout from '@/views/Checkout';
import _ from 'lodash';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      id: 'Home',
      component: Home,
    },
    {
      path: '/checkout/:cartId',
      name: 'Checkout',
      component: Checkout,
      props: true,
    },
  ],
});


class StockError extends Error {
  constructor(productId) {
    super('Error stock');
    this.productId = productId;
  }
}

export default StockError;

import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cart: [],
  },
  getters: {
    getCart(state) {
      return state.cart;
    },
  },
  actions: {
    addToCart({ commit }, itemId) {
      commit('addToCart', itemId);
    },
  },
  mutations: {
    addToCart(state, itemId) {
      const item = state.cart.find(it => it.itemId === itemId);
      if (item) {
        item.units += 1;
      } else {
        state.cart.push({
          itemId,
          units: 1,
        });

      }

    },
  },
});
